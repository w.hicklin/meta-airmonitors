LICENSE = "Unknown"
LIC_FILES_CHKSUM = "file://license;md5=de17abf1897f88515cafc37d8dc25e39"

SRC_URI = "git://github.com/hicklin/NemaTode.git;protocol=https"

PV = "1.0.0+git${SRCPV}"
SRCREV = "d08022ca83b7fd056b431a26fa1239ec58779e32"

S = "${WORKDIR}/git"

inherit cmake
