DESCRIPTION = "wxWidgets is a cross platform application framework utilizing native widgets."
HOMEPAGE = "http://www.wxwidgets.org"

LICENSE = "WXwindows"
LIC_FILES_CHKSUM = "file://docs/licence.txt;md5=18346072db6eb834b6edbd2cdc4f109b"

# DEPENDS = "webkit-gtk gstreamer gtk+ jpeg tiff libpng zlib expat libxinerama libglu"
# DEPENDS = "cppunit libsdl gtk+ gtk+3 autoconf-archive libglu bakefile"
DEPENDS = "gtk+ libxtst"
RDEPENDS_${PN} = "gtk+"

SRC_URI = "https://github.com/wxWidgets/wxWidgets/releases/download/v${PV}/wxWidgets-${PV}.tar.bz2"
SRC_URI[md5sum] = "3374d7841a32772a2a0f66b914fcaf09"
SRC_URI[sha256sum] = "4cb8d23d70f9261debf7d6cfeca667fc0a7d2b6565adb8f1c484f9b674f1f27a"

S = "${WORKDIR}/wxWidgets-${PV}"

inherit binconfig autotools pkgconfig

PROVIDES = "wxwidgets"

# Note: libtiff causes an "is unsafe for cross-compilation" issue due to
# "-L/usr/lib/nvidia". Hence it is being removed here.
EXTRA_OECONF = "--enable-option-checking \
                --without-libpng \
                --without-libjpeg \
                --without-libtiff \
                --disable-gif \
                --disable-pcx \
                --disable-iff \
                --disable-svg\
                "

autotools_do_configure () {
  oe_runconf
}

do_compile_append () {
  # Point to the right include and lib directories.
  sed -i -e '/includedir=".*"/i WX_CONFIG_PATH="$( cd "$(dirname "$0")" ; pwd -P )"' ${B}/lib/wx/config/*
  sed -i -e 's,includedir=".*",includedir=$WX_CONFIG_PATH/../include,g' ${B}/lib/wx/config/*
  sed -i -e 's,libdir=".*",libdir=$WX_CONFIG_PATH/../lib,g' ${B}/lib/wx/config/*
  sed -i -e 's,bindir=".*",bindir=$WX_CONFIG_PATH/../bin,g' ${B}/lib/wx/config/*
}

SYSROOT_PREPROCESS_FUNCS += "wxwidgets_sysroot_preprocess"
wxwidgets_sysroot_preprocess () {
  # Make wx-config a symbolic link to the base-unicode-3.1 file in the sysroots.
  mkdir -p ${SYSROOT_DESTDIR}${bindir}
  cd ${SYSROOT_DESTDIR}${bindir}
  rm -f wx-config
	ln -sf ../lib/wx/config/arm-fslc-linux-gnueabi-gtk2-unicode-3.1 wx-config
}

INSANE_SKIP_${PN} = "dev-so"

FILES_${PN} = "${bindir}/* ${libdir}/wx/* ${libdir}/libwx*"
FILES_${PN}-dev = "${datadir}/bakefile/* ${datadir}/aclocal/* ${prefix}/src/* ${includedir}/*"
FILES_${PN}-dbg = "${libdir}/.debug/libwx* ${bindir}/.debug/*"
