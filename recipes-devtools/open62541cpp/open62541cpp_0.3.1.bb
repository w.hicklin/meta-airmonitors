LICENSE = "MPL-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=815ca599c9df247a0c7f619bab123dad"

SRC_URI = "git://github.com/hicklin/open62541cpp.git;protocol=https"

PV = "0.3.1+git${SRCPV}"
SRCREV = "25441cf8d8f8bda033d4ab82a5fef2bcf1563168"

S = "${WORKDIR}/git"

DEPENDS = "boost"

inherit cmake
