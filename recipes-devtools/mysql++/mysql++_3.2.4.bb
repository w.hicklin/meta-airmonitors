LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING.txt;md5=180d6523109498d7cc2af2dfb61f49be \
                    file://LICENSE.txt;md5=a6f89e2100d9b6cdffcea4f398e37343 \
                    file://doc/userman/licenses.dbx;md5=4cba692a366e2c9c52df0ad0c70ddbee \
                    file://doc/userman/LICENSE.txt;md5=dc504d5b8a70eebe55a686d98d4486ed \
                    file://doc/html/userman/licenses.html;md5=534915109519a24954cc7aae945ff969"

SRC_URI = "https://tangentsoft.com/mysqlpp/releases/mysql%2B%2B-${PV}.tar.gz \
           file://0001-included-the-cstring-library.patch \
           "
SRC_URI[md5sum] = "cda38b5ecc0117de91f7c42292dd1e79"
SRC_URI[sha256sum] = "6b60727b36b1fed78e0c935d6510b31821c71ec0076967cd9fa5ecf4320d7d63"

S = "${WORKDIR}/mysql++-${PV}"

DEPENDS = "libmysqlclient"

inherit autotools-brokensep

EXTRA_OECONF = " \
  --with-mysql=${STAGING_EXECPREFIXDIR} \
  "

# Broken autotools :/
do_configure() {
	oe_runconf
}
