LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=4fbd65380cdd255951079008b364516c"

SRC_URI = "git://github.com/hicklin/ObjectPropertyTree.git;protocol=https"

PV = "1.0.1+git${SRCPV}"
SRCREV = "b6e9c543b0ae88af6bb19d4949a83414c0a038e2"

S = "${WORKDIR}/git"

DEPENDS = "boost"

inherit cmake
