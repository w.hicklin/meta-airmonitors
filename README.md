# The meta-airmonitors layer

This layer is maintained my Air Monitors Ltd. It contains recipes of
open-source libraries which did not exist at the time when they were needed for
internal projects.

## Dependencies

This layer depends on the [meta-oe](git://git.openembedded.org/meta-openembedded) layer.

## Patches

Please submit any patches against the meta-airmonitors layer to the maintainer.

Maintainer: William Hicklin <williamhicklin@airmonitors.uk>

## Adding the meta-airmonitors layer to your build

Run 'bitbake-layers add-layer meta-airmonitors'
