LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://package.xml;beginline=13;endline=13;md5=58e54c03ca7f821dd3967e2a2cd1596e"

SRC_URI = "git://github.com/wjwwood/serial.git;protocol=https"

PV = "1.2.1+git${SRCPV}"
SRCREV = "683e12d2f6a26c80bfa07f276845be618237ae5b"

S = "${WORKDIR}/git"

inherit catkin

# This is a Catkin (ROS) based recipe
# ROS package.xml format version 1

SUMMARY = "ROS package serial"
DESCRIPTION = "Serial is a cross-platform, simple to use library for using serial ports on computers. This library provides a C++, object oriented interface for interacting with RS-232 like devices on Linux and Windows."
HOMEPAGE = "http://wjwwood.github.com/serial/"
# ROS_BUGTRACKER = "https://github.com/wjwwood/serial/issues"
# SRC_URI = "https://github.com/wjwwood/serial"
# ROS_AUTHOR = "William Woodall <wjwwood@gmail.com>"
# ROS_AUTHOR += "John Harrison <ash.gti@gmail.com>"
# ROS_MAINTAINER = "William Woodall <william@osrfoundation.org>"
SECTION = "devel"

# Install the serialConfig.cmake file in the appropriate recipe sysroot directory.
SYSROOT_PREPROCESS_FUNCS += "cmake_find_sysroot_preprocess"
cmake_find_sysroot_preprocess () {
  install -d ${SYSROOT_DESTDIR}${libdir}/cmake/serial
  install -m 0644 ${SYSROOT_DESTDIR}${ros_datadir}/serial/cmake/serialConfig.cmake ${SYSROOT_DESTDIR}${libdir}/cmake/serial
}
